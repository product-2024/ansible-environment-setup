# ansible-production-setup

## 環境セットアップ

  1. オンライン環境におけるAnsibleプレイブック実行

      ```bash
      ansible-playbook site.yml --tags=online
      ```

  2. オフライン環境におけるAnsibleプレイブック実行

      ```bash
      ansible-playbook site.yml --tags=<各種role>
      ```

## 注意事項

- dockerインストール後、gitインストールする場合

以下を実行する。

```bash
scl enable devtoolset-12 bash
```

```bash
ansible-playbook site.yml --tags=git
```
